import { Injectable } from '@nestjs/common';
import { PostRepository } from './post.repository';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PostService {
  constructor(  @InjectRepository(PostRepository) private readonly postRepository:PostRepository) {
  }

  async getPosts() {
    return this.postRepository.find();
  }
}
