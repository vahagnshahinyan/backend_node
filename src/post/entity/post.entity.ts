import { Column, Entity, JoinColumn, ManyToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CategoryEntity } from '../../category/entity/category.entity';
import { JoinTable } from 'typeorm/browser';
@Entity('post')
export class PostEntity {

  @PrimaryGeneratedColumn()
  id:number;

  @Column()
  title:string;

  @Column()
  description:string;

  @ManyToMany(()=>CategoryEntity, categoryEntity=>categoryEntity.id)
  @JoinColumn()
  categories:CategoryEntity[];
}
