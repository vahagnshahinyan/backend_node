import { Injectable, UnauthorizedException } from '@nestjs/common';
import { USER_NOT_FOUND_ERROR, WRONG_PASSWORD_ERROR } from './auth.constants';
import { compare } from 'bcryptjs';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { UserEntity } from '../users/user.entity';

@Injectable()
export class AuthService {
  
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService
  ) {
  }
  
  async validateUser(email: string, password: string): Promise<Omit<UserEntity, 'password'>> {
    
    const user = await this.userService.findUser(email);
    if (!user) {
      throw new UnauthorizedException(USER_NOT_FOUND_ERROR);
    }
    const isCorrectPassword = await compare(password, user.password);
    if (!isCorrectPassword) {
      throw new UnauthorizedException(WRONG_PASSWORD_ERROR);
    }
    return {
      email: user.email,
      id: user.id
    };
  }
  
  async login(email: string, id: number) {
    const payload = { email,id };
    return {
      access_token: await this.jwtService.signAsync(payload)
    };
  }
  
}
