import { userType } from 'src/users/types/user.types';

export interface UserResponseInterface {
  user: userType & { token: string };
}
