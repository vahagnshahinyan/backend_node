import { IsString } from 'class-validator';

export class UserUpdateDto {
  @IsString()
  login: string;

  @IsString()
  password: string;

}
