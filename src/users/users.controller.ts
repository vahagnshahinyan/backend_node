import { Body, Controller, Post, Put, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { UserResponseInterface } from './types/userResponse.interface';
import { UsersService } from './users.service';
import { UserDto } from './dto/user.dto';

@Controller('user')
export class UsersController {
  constructor(private readonly userService: UsersService) {
  }
  
  @UsePipes(new ValidationPipe())
  @Post('create')
  async createUser(@Body() createUserDto: UserDto): Promise<UserResponseInterface> {
    const user = await this.userService.createUser(createUserDto);
    return this.userService.buildUserResponse(user);
  }
  

}
