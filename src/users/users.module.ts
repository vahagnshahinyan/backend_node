import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { UsersController } from './users.controller';
import { ConfigService } from '@nestjs/config';

@Module({

  imports: [
	TypeOrmModule.forFeature([UserEntity]),
  ],
  providers: [UsersService,ConfigService],
  controllers: [UsersController],
  exports:[UsersService]
})
export class UsersModule {
}

