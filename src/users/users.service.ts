import { Injectable, UnauthorizedException } from '@nestjs/common';
import { genSalt, hash } from 'bcryptjs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import { UserUpdateDto } from './dto/user-update.dto';
import { sign } from 'jsonwebtoken';
import { ConfigService } from '@nestjs/config';
import { UserResponseInterface } from './types/userResponse.interface';
import { UserDto } from './dto/user.dto';
import { WRONG_PASSWORD_ERROR } from 'src/auth/auth.constants';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly configService: ConfigService
  ) {
  }
  
  async createUser(dto: UserDto) {
    
    if (dto.password !==dto.repeatPassword){
        throw new UnauthorizedException(WRONG_PASSWORD_ERROR);
    }
    
    const salt = await genSalt(10);
    const newUser = new UserEntity();
    newUser.email = dto.login;
    newUser.password = await hash(dto.password, salt);
    
    return this.userRepository.save(newUser);
    
    
  }
  
  async findUser(email: string) {
    return this.userRepository.findOne({ email });
  }
  
  
  async updateUser(
    userId: number,
    updateUserDto: UserUpdateDto
  ): Promise<UserEntity> {
    const user = await this.userRepository.findOne(userId);
    Object.assign(user, updateUserDto);
    return this.userRepository.save(user);
  }
  
  //
  generateJwt(user: UserEntity): string {
    return sign(
      {
        id: user.id,
        email: user.email
      },
      this.configService.get('JWT_SECRET')
    );
  }
  
  //
  buildUserResponse(user: UserEntity): UserResponseInterface {
    return {
      user: {
        ...user,
        token: this.generateJwt(user)
      }
    };
  }
  
}
