import { Column, Entity, JoinColumn, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { PostEntity } from '../../post/entity/post.entity';
@Entity('category')
export class CategoryEntity {
  @PrimaryGeneratedColumn()
  id:number;

  @Column()
  title:string;


  @ManyToMany(()=>PostEntity, postEntity=>postEntity.id)
  @JoinColumn()
  posts:PostEntity[];
}
