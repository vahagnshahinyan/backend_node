import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './users/user.entity';
import { FileModule } from './file/file.module';
import { FileEntity } from 'src/file/entities/file.entity';
import { PostModule } from './post/post.module';
import { CategoryModule } from './category/category.module';
import { PostEntity } from './post/entity/post.entity';
import { CategoryEntity } from './category/entity/category.entity';
import {join} from 'path'

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get('POSTGRES_HOST'),
          port: configService.get('POSTGRES_PORT'),
          username: configService.get('POSTGRES_USER'),
          password: configService.get('POSTGRES_PASSWORD'),
          database: configService.get('POSTGRES_DB'),
          // entities: [join(__dirname, '**', '*.entity.{ts,js}')],

          entities: [
            UserEntity,
            FileEntity,
            PostEntity,
            CategoryEntity
          ],
          synchronize: true
        };
      }
    }),
    AuthModule,
    UsersModule,
    FileModule,
    PostModule,
    CategoryModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {
}
