import {extname} from 'path';
import * as fs from 'fs';


export const editFileName = (req, file, callback) => {
    const name = file.originalname.split('.')[0];
    const fileExtName = extname(file.originalname);
    const setFileName = new Date().getTime() + '_' + Math.round(1000 * Math.random()) + fileExtName;
    callback(null, `${setFileName}`);
};


const datePath = () => {
    const today = new Date();
    let dd: number | string = today.getDate();
    let mm: number | string = today.getMonth() + 1; //January is 0!
    const yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return yyyy + '/' + mm + '/' + dd;
};


export const uploadDir = () => {
    const dir = './upload/' + datePath() + '';
    createDir(dir);
    return dir;
};


function createDir(dir) {
    const array = dir.split('/');
    let path = '';
    array.forEach((d) => {
        path += d + '/';
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
    });
}
