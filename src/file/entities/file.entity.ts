import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserEntity } from 'src/users/user.entity';
// import { UserEntity } from 'src/users/user.entity';

@Entity({name:'file'})
export class FileEntity {
  @PrimaryGeneratedColumn()
  id: number;
  
  @Column()
  fieldname: string;
  
  @Column()
  originalname: string;
  
  @Column()
  encoding: string;
  
  @Column()
  mimetype: string;
  
  @Column()
  destination: string;
  
  @Column()
  filename: string;
  
  @Column()
  path: string;
  
  @Column()
  size: number;
  
  
  @OneToOne(() => UserEntity)
  @JoinColumn()
  user: UserEntity;
  
}
