import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { FileController } from './file.controller';
import { FileService } from './file.service';
import { FileEntity } from 'src/file/entities/file.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [
    AuthModule,
    TypeOrmModule.forFeature([FileEntity]),
    MulterModule.register({
      dest: './upload',
      limits: {
        fileSize: 5000000
      }
    })
  ],
  controllers: [FileController],
  providers: [FileService]
})
export class FileModule {

}


