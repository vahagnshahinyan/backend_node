import {Injectable} from '@nestjs/common';
import {CreateFileDto} from 'src/file/dto/create-file.dto';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {FileEntity} from 'src/file/entities/file.entity';

@Injectable()
export class FileService {

    constructor(@InjectRepository(FileEntity) private fileRepository: Repository<FileEntity>) {
    }

    async create(createFileDto: CreateFileDto) {
        const image = this.fileRepository.create(createFileDto);
        await this.fileRepository.save(image);
        return image;
    }

    findAll() {
        return `This action returns all files`;
    }

    findOne(id: number) {
        return `This action returns a #${id} file`;
    }


}
