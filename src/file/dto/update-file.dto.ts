import { PartialType } from '@nestjs/swagger';
import { CreateFileDto } from 'src/file/dto/create-file.dto';

export class UpdateFileDto extends PartialType(CreateFileDto) {
}
