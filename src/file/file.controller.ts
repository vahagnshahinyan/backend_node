import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Res,
  UploadedFile,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
  Request
} from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { FileService } from 'src/file/file.service';
import { CreateFileDto } from 'src/file/dto/create-file.dto';
import { editFileName, uploadDir } from 'src/utils/file-upload.utils';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';

@Controller('file')
export class FileController {

  constructor(private readonly fileService: FileService,
  ) {
  }

  @Post()
  create(@Body() createFileDto: CreateFileDto) {
    return this.fileService.create(createFileDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll() {
    return this.fileService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fileService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, uploadDir());
        },
        filename: editFileName
      })
    })
  )
  async uploadedFile(@UploadedFile() file, @Request() req) {



    return this.fileService.create(file);
  }

  @UseGuards(JwtAuthGuard)
  @Post('uploads')
  @UseInterceptors(
    FilesInterceptor('files', 20, {
      storage: diskStorage({
        destination: (req, file, cb) => {
          cb(null, uploadDir());
        },
        filename: editFileName
      })
    })
  )
  async uploadMultipleFiles(@UploadedFiles() files) {
    const fileServiceArr = [];
    for (const file of files) {
      fileServiceArr.push(this.fileService.create(file));
    }
    return Promise.all(fileServiceArr);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':filepath')
  seeUploadedFile(@Param('filepath') image, @Res() res) {
    return res.sendFile(image, { root: './upload' });
  }


}
