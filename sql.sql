create table news.category
(
    id    serial
        constraint category_pk
            primary key,
    title varchar
);

alter table news.category
    owner to vahag;




create table news.category_post
(
    id          serial
        constraint category_post_pk
            primary key,
    post_id     integer not null
        constraint category_post_post_id_fk
            references news.post,
    category_id integer not null
        constraint category_post_category_id_fk
            references news.category
);

alter table news.category_post
    owner to vahag;



create table news.image
(
    id         serial
        constraint image_pk
            primary key,
    image_path varchar,
    image_alt  varchar
);

alter table news.image
    owner to vahag;



create table news.post
(
    id          serial
        constraint post_pk
            primary key,
    title       varchar,
    description text,
    image_id    integer
        constraint post_image_id_fk
            references news.image
);

alter table news.post
    owner to vahag;




create table news.post_image
(
    id       serial
        constraint post_image_pk
            primary key,
    image_id integer not null
        constraint post_image_image_id_fk
            references news.image,
    post_id  integer not null
        constraint post_image_post_id_fk
            references news.post
);

alter table news.post_image
    owner to vahag;

